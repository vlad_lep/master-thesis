function [ data] = remove_empty_cells( data )
%REMOVE_EMPTY_CELLS Summary of this function goes here
%   Detailed explanation goes here
    contor = size(data,2);
    while contor >0 && isempty(data(contor).name) 
        data(contor)=[];
        contor= contor -1;
    end

end

