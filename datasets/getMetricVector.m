function [ code ] = getMetricVector( path_to_file, metric_name)
    xDoc = xmlread(path_to_file);
    allListitems = xDoc.getElementsByTagName(metric_name);
    numElems = allListitems.getLength;
    code = [];
    for i=1:numElems
        child = allListitems.item(i-1);
        code = [code;str2num(child.getFirstChild.getData)];

    end
end

