projectFolder = './projects';
[activity_data, size_data, errors_data] = parseXML(projectFolder);
activity_data = remove_empty_cells(activity_data);
size_data = remove_empty_cells(size_data);
errors_data = remove_empty_cells(errors_data);
[activity_data, size_data, errors_data] = remove_empty_projects( activity_data, size_data, errors_data);
