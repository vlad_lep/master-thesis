function [ activity_facts, size_facts, errors] = remove_empty_projects( activity_facts, size_facts, errors)
%REMOVE_EMPTY_PROJECTS Filter out the porjects that have no activity or no
% size facts
    for i=size(size_facts,2):-1:1
        if isempty(size_facts(i).code)
            %remove also activity for this project
            for j=size(activity_facts,2):-1:1
                if strcmp(size_facts(i).name, activity_facts(j).name)
                    errors = add_error(errors, size_facts(i).name, 'empty size facts');
                    size_facts(i) = [];
                    activity_facts(j) = [];
                end
            end
        end
    end
end

