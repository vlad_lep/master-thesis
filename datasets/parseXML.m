function [ projects_data, size_data, data_proc_errors ] = parseXML(projectFolder)

    imported_file = 'ActivityFacts.xml';
    listing = dir(projectFolder);
    project_cell = cell(1,2 * (size(listing,1)-2));
    data_proc_errors = struct('name', project_cell, 'error', project_cell);
    allocCell = cell(1, size(listing,1)-2);
    projects_data = struct( 'name', allocCell, 'code_added', allocCell, ...
                            'code_removed',allocCell,...
                            'comments_added', allocCell, 'comments_removed', allocCell, ...
                            'blanks_added', allocCell, 'blanks_removed', allocCell, ...
                            'contributors', allocCell, 'commits', allocCell);    
    contor = 1;
    errors_count = 1;
    
    for i=1:size(listing) 
    if ~(strcmp(listing(i).name,'.')) && ~(strcmp(listing(i).name,'..'))
        path_to_project = strcat(projectFolder,'/', listing(i).name);
        path_to_file = strcat(path_to_project, '/', imported_file);
        if exist(path_to_file, 'file')
            if mod(i,200)==0
                i
            end
            projects_data(contor).name = listing(i).name;
            projects_data(contor).code_added = getMetricVector(path_to_file, 'code_added');
            projects_data(contor).code_removed = getMetricVector(path_to_file, 'code_removed');
            projects_data(contor).comments_added = getMetricVector(path_to_file, 'comments_added');
            projects_data(contor).comments_removed = getMetricVector(path_to_file, 'comments_removed');
            projects_data(contor).blanks_added = getMetricVector(path_to_file, 'blanks_added');
            projects_data(contor).blanks_removed = getMetricVector(path_to_file, 'blanks_removed');
            projects_data(contor).contributors = getMetricVector(path_to_file, 'contributors');
            projects_data(contor).commits = getMetricVector(path_to_file, 'commits');
            contor = contor + 1;
        else
            data_proc_errors(errors_count).name = listing(i).name;
            data_proc_errors(errors_count).error = 'missing ActiviyFacts file';
            errors_count= errors_count + 1;
        end
            
    end
    end

% --------------------------------- import Size Facts --------------------%    
    imported_file = 'SizeFacts.xml';
    allocCell = cell(1, size(listing, 1)-2);
    size_data = struct( 'name', allocCell, 'code', allocCell, ...
                            'comments',allocCell,...
                            'comment_ratio', allocCell, 'man_months', allocCell, ...
                            'blanks', allocCell, 'commits', allocCell);
    contor = 1;

    for i=1:size(listing) 
    if ~(strcmp(listing(i).name,'.')) && ~(strcmp(listing(i).name,'..'))
        path_to_project = strcat(projectFolder,'/', listing(i).name);
        path_to_file = strcat(path_to_project, '/', imported_file);        
        if exist(path_to_file, 'file')
            if mod(i,300)==0
                i
            end
            size_data(contor).name = listing(i).name;
            size_data(contor).code = getMetricVector(path_to_file, 'code');
            size_data(contor).comments = getMetricVector(path_to_file, 'comments');
            size_data(contor).comment_ratio = getMetricVector(path_to_file, 'comment_ratio');
            size_data(contor).man_months = getMetricVector(path_to_file, 'man_months');
            size_data(contor).blanks = getMetricVector(path_to_file, 'blanks');
            size_data(contor).commits = getMetricVector(path_to_file, 'commits');
            contor = contor + 1;
        else
             data_proc_errors(errors_count).name = listing(i).name;
            data_proc_errors(errors_count).error = 'missing SizeFacts file';
            errors_count= errors_count + 1;
        end
    end
    end

  
end