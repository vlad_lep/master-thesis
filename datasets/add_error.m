function [ errors ] = add_error( errors, name, desc)
%ADD_ERRORS Add a new error to the array of errors
    no_errors = size(errors,2);
    new_cell = cell(1,1);
    new_struct = struct( 'name', new_cell, 'error', new_cell);
    new_struct.name = name ;
    new_struct.error = desc;
    errors(no_errors+1) = new_struct;
end

