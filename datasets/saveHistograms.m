% Saves a histogram plot for each dimension of the data. 
% data:  the n x m matrix of data, for which m number of plots will be obtained 
% name: the name of the images. The dimension number will be added at the
% end

function  saveHistograms( data, name)
for i=1:size(data,2)
    
    hist(data(:,i),100);
    str  = strcat(name,num2str(i));
    str  = strcat('/data/',str);
    print('-dpng', str)
   
end

