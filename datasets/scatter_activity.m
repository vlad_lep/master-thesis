function [ input_data] = scatter_activity( activity_data)
%SCATTER_ACTIVITY Summary of this function goes here
%   Detailed explanation goes here
    
    new_len = 200;
%     for i=1:new_len
%         subset_data(i) = activity_data(i);
%     end
    count = 1;
    input_data = [];
  
    for i=1:new_len
        if mod(i,100)==0
            
            i
        end
        hold all;
        for j=1:size(activity_data(i).contributors,1)
            if mod(j,12) ==0
                break;
            else
                scatter(activity_data(i).contributors(j), activity_data(i).commits(j));
%                 size(input_data)
%                 size([activity_data(i).contributors(j), activity_data(i).commits(j)])
                input_data(count,:) = [activity_data(i).contributors(j), activity_data(i).commits(j)];
                count = count+1;
            end
    end

%     scatter(subset_data.contributors, subset_data.code_added);
end

