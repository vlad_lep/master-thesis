\chapter{Approaches to noise detection}

We can see the task of detecting noise in two different ways, depending on the data we have. 
\begin{enumerate}
\item If we have pre-labeled data, which describes in advance whether each instance is noise or not, we can treat our problem as a \textit{classification task}. We train our classifier and predict the class for each instance.  Different options can be used for the classifiers, e.g. Support Vector Machines(SVM), Neural Networks, Decision Trees, K Nearest Neighbor, etc. 

\item If we do not have prior knowledge of the data, no labels, we can not use a classifier. In this situation the approaches are similar to \textit{unsupervised clustering} and the solutions is to model somehow the ``normal'' state, according to the training data, and consider the instances that are furthest from normal as suspect of having noise. Such methods are K-Means, Gaussian Model Mixtures, Linear Regression, etc.
\end{enumerate}

The second problem is harder than the first, since we do not know in advance what are the possible problems that can appear. We can see the first problem as a sub-problem of the second, as all the methods used to find noise in non-labeled data can be applied on labeled collections.

In our research, using the Ohloh datasets, we have no labels for our instances, we do not even know what is the distribution of our data and what are the possible errors that could be in our dataset. Therefore, we are facing the second class of the problem and our aim is to learn what is normal for our dataset and then highlight the points that are furthest from normal, as suspect of noise. 

In this chapter, we will shortly explain how can we learn what is normal in our data, how do Gaussian Processes work, how we used them in our research to detect noise in our dataset and why did we chose them. We will limit our examples to two dimensional data, as visual representations can be easily generated in this case.

\section{Examples of how to define what is normal in data}

\subsection*{K-means}

To learn what is normal in a dataset, different models can be used. A very simple, but actually powerful technique for noise detection is K-means, used in \cite{Kmeans1, Kmeans2, Kmeans3}. K-means is a clustering algorithm that splits the data into K clusters, each instance belonging to the nearest mean. To calculate the means that best fit our model is NP-hard, but efficient approximation methods are used that converge fast to a local minimum. In Figure \ref{fig:kmeans}, we see on the left how that specific data would be split in two clusters based on the two centroids. The total space is split by a line, what is on one side is green and what is on the other side is brown. This is how K-means would be used for clustering. 

For noise detection, we should calculate the means and afterwards select a variance around the means that we consider normal. The points that are close to one of the means, are normal points while the other ones are noise. This is represented in the picture, where the brown points inside the circles are normal, while the green ones are noise.
By using mixtures of Gaussian, we can change the circles in eclipses and create a more flexible model. Nevertheless, the principles and the form of the model is the same.

\begin{figure}[h!]
	\centering
		\includegraphics[scale = 0.5]{./img/gp/kmeans.png}
	\caption{(left) Clustering with K-means. (right) Noise detection using K-means}
	\label{fig:kmeans}
\end{figure}

\subsection*{Linear Regression}
Another approach, is to learn a correlation between the two features, which is actually learning to predict one feature from the other. 
Linear regression was used in literature and some examples are decribed in \cite{SurveyOutlierTech}. We will use it also to exemplify.

We do not have a predefined class for our instances, therefore we choose one of the attributes to be the class that we want to predict.  In Figure \ref{fig:liniar}, after we learn the linear function we can make predictions for new points, e.g. the red point in the graph on the left. It is probably not an exact prediction but it is the estimation we can make, based on what we learned. This is how linear regression would normally be used.

In our case we are not interested in the predictions, we need to detect noise. To judge if a point is noisy or not, we should calculate the Euclidean distance between its real value and its prediction and if it is higher than a prior selected tolerance we should report it as possible noise. In this approach, we are generating a model that varies around a linear function with a certain variance. 

\begin{figure}[h!]
	\centering
		\includegraphics[scale = 0.5]{./img/gp/liniar.png}
	\caption{(left) Linear regression. (right) Noise detection using linear regression }
	\label{fig:liniar}
\end{figure}

The two models are different, each being better suited for particular domains. K-means, has the advantage that it can handle data that is grouped around some points, anywhere in space but it would have difficulties to model linear functions for example. While linear regression can have difficulties to fit non-linear data, e.g. data distributed on a circle.

\subsection*{Gaussian Processes}
Because we do not have prior information about the Oholoh dataset and we want to be as flexible as possible, we chose Gaussian Processes(GPs). 
GPs define a probability distribution over non-linear functions and the model they create could be viewed as a non-linear mean function that has an adaptable Gaussian variance around it. The way our model looks can be changed by the choices we make for the covariance function and mean. Models like the ones presented in Figure \ref{fig:gp_models} can be obtained. Some are more smooth or more continuous than others.

\begin{figure}[h!]
	\centering
    \begin{minipage}[b]{.35\textwidth}
    	\centering
        \includegraphics[width=\textwidth]{./img/gp/Gp_1.png}
    \end{minipage}
	\qquad
    \begin{minipage}[b]{.35\textwidth}
    	\centering
        \includegraphics[width=\textwidth]{./img/gp/Gp_2.png}
    \end{minipage}
    \qquad
    \caption{GP model with different covariance functions \cite{Matlabcode}}
    \label{fig:gp_models}
\end{figure}


\section{Gaussian Processes basics}

The study of Gaussian Processes and their use for prediction is far from new [34]. The astronomer Thiele used Gaussian Processes since 1880 \cite{Lauritzen} for time series analysis, while in 1948 Wiener-Kolmogorov proposed predictions theories using Gaussian Processes for trajectories of military targets \cite{WienerK}.
 In this chapter we will explain concisely the mathematical background of the Gaussian Processes, using the introduction written by Boyle in \cite{ThesisPhilip} as outline. The deeper mathematical concepts are explained in Mackay \cite{IntroMackay} and Rasmussen \cite{IntroRasmusses}. For more details please read these papers, follow the online lectures \cite{videoLecture} or read some of the other materials \cite{GPbook,  Matlabcode}

In machine learning, the use of Gaussian Processes has gained more interest with the introduction of back-propagation for learning in neural networks. Neural networks are composed of connected layers of non-linear functions (neurons), each connection having a weight that establishes its importance. If we view this from a Bayesian perspective, these weights are basically defining a prior probability over the non-liniear functions. The back-propagation training methods from neural networks adapts these weights, which basically can be seen as defining a posterior probability for the non-linear function. The obtained model is a composition of non-linear function with certain probabilities learned while training. Neal \cite{Neal} showed that Gaussian Processes can substitute the parameterized normal neural networks, facilitating simpler computations using matrices instead of more complicated parameter optimization for neural networks. He showed that the prior distribution of the non-linear functions are a subclass of probability distributions of Gaussian Processes, while the hyperparameters of the neural network determine the lengthscale of the Gaussian process. 


In our thesis we use Gaussian Processes for regression. We describe the principles behind GPs starting from the notion of Parametric Regression, then explaining Bayesian Regression and last Gaussian Processes.

\subsubsection*{Parametric Regression}

We will use $X$ to define all the $N$ input vectors $x_{(i)}$, each $x_{(i)}$ of dimension $I$, and real numbers $Y$ for targets $y_i$ corresponding to the inputs. A regression problems presumes to learn a mapping between the inputs $X$ and the targets $Y$ .

In parametric regression, our mapping is a function $f(x;w)$ defined in terms of the parameters $w$. The objective is to find the parameters that "best" describe the data. 

One way to compare what is a better description of our data, would be to minimize a cost function. A typical cost function is the least squares, which basically measures the square distance between the predictions and targets.
\begin{align}
	L(w) = \sum^N_{i=1} (y_i - f(x_i;w))^2
\end{align}

In this approach, the parameters that generate the lower value for the cost function, created the "best" model. As examples of parametric approaches we can think of polynomial regression, where the parameters are the coefficients of the polynomial, or fed-forwards neural networks, where the parameters are the weights.

There are three shortcomings of using cost functions. First, is the lack of error bars for predictions. This means that we obtain just a predicted value, but no measurement of how likely is it that this prediction is correct. Secondly, we need to initialize the parameters $w$, which can have a big impact on the resulting model. Third, we need to deal with problems of overfitting. Using the least squares cost function, we are are minimizing the function errors on the training data. We can obtain very low values for $L(w)$, close to zero for some domains, if we use more complex models (e.g. higher order polynomials) but this will most likely give bad performance on the test data because we overfit, learn details of the training data instead of general properties. If we choose simpler models (e.g. lower order polynomials) we might not be able to learn enough about our data and therefore also have bad prediction performance. The solutions is somewhere in between.

A second approach to compare what model is better, is to add a noise model besides the function:
\begin{align}
	y_i = f(x_i;w) + \epsilon_i
\end{align}
where $\epsilon_i$ is independent noise, usually of Gaussian distribution $N(0,\sigma)$.
Now we can use a likelihood function $p(y|X, w, \sigma^2)$ as in \cite{ThesisPhilip}, to obtain error bars for our predictions.

\subsubsection*{Bayesian Regression} We can use, for these parametric approaches, Bayesian theory to minimize the overfitting problem. With Bayes rule we can calculate the \textit{posterior} distribution \cite{ThesisPhilip}:

\begin{align}
	p(w|y, X, \sigma^2) = \frac{ p(y|X, w, \sigma^2) p(w)}{p(y|X, \sigma^2)}
\end{align}
where p(w) is the prior density function and it is set prior, based on what we think the mode should look like for the specific data. $ p(y|X, w, \sigma^2)$ is the likelihood function and $p(y|X, \sigma^2)$ is the marginal likelihood that we calculate by integrating over the parameters $w$.

To make the prediction $y_*$ for $x_*$, we calculate the probability (error bars) using the formula from \cite{ThesisPhilip}:
\begin{align}
	p(y_* | x_*, y, X, \sigma^2) = \int p(y_*|x_*, W, \sigma^2)p(w|y,X,\sigma^2) dw
\end{align}
The formula shows one important thing: that not only a single set of parameters contributes to the predictions, but all parameters do; ``the predictive contribution from a particular set of parameters
is weighted by its posterior probability" \cite{ThesisPhilip}. Combining models with different parameters to obtain the final one makes the generated model less likely to \textit{overfit} our training data.

\subsubsection*{Gaussian processes}
Gaussian Processes can be considered replacements for many parametric models. The major advantage is that the functions are not explicitly parametrized, making them more useful in situations were we do not know beforehand what the data looks like and how to choose the parameters. 

We define a probability density function $p(f)$ over a function space $F$. We then sample function $f$, $f: X \rightarrow \mathbb{R}$ from $F$, according to the distribution $p(f)$.  If we calculated $f(x)$, where $x\in X$ for multiple sampled functions $f$ we obtain a random variable $f(x)$ with a certain distribution. An example is shown in Figure \ref{gp:functions} from \cite{ThesisPhilip}.
\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth]{./img/gp/multi_fct.png}
	\caption{(Left)Multiple sampled functions $f$. (Right) Distribution of $f(1)$ after normalizing the histogram of 1000 sampled functions at $f(1)$ \cite{ThesisPhilip}}
\label{gp:functions}
\end{figure}

If $x \in X$ and the distribution of $f \in \mathbb{R}$ for any finite N is a multivariate Gaussian, we call this stochastic process a \textit{Gaussian process}.

Gaussian processes are non-parametric methods and are fully defined by a \textit{mean function} $\mu$ and a \textit{covariance function} $C(x,x')$. The prior mean function  $\mu$ is usually unknown, therefore it is set to zero and learned from the training data. The covariance function must be chosen by us and it defines how smooth the models looks like.

Having these two set, we have a prior for our Gaussian Processes. Nevertheless, the prior specifies only the properties of the functions and does not depend on the training data. For this hyperparameters $\theta$ are introduced to describe the mean and the covariance functions, making them more adaptable. By inferring $\theta$ from the training data we are afterwards able to make predictions. 

The mathematical theory of how the hyperparameters are learned from the training data and how the predictions are made using matrix multiplications, will not be presented in this thesis. Please read the papers suggested at the beginning of this Chapter for more details. We will not present some graphical representations of Gaussian Processes produced with different covariance functions.

\paragraph{Covariance functions}
Choosing the covariance function seems to have the greatest impact on the generated mode. To be a valid covariance function it must be positive semidefined which means it has to respect the condition: 

\begin{align}
	\int C(x,x') f(x) f(x') du(x) dy(x') \geq 0
\end{align}
Some of the used covariance functions are presented in Table \ref{tab:cov_functions}.

\begin{table}[h!]
  \begin{center}
	\begin{tabular}{ | l |l | }
	  \hline
	  covariance function  & expression \\
	  \hline
	constant & $ \sigma^2_0$ \\
	linear & $ \sum_{d=1}^D \sigma^2_d x_d x'_d$ \\
	polynomial & $ (x x' +\sigma_0^2)^p $ \\
	squared exponential & $ exp(-\frac{r^2}{2l^2}) $ \\
	Matern & $ \frac{1}{2^{v-1} \Gamma(v)} ( \frac{\sqrt{2v}}{l}r )^v K_v (\frac{\sqrt{2v}}{l}r) $ \\
	exponential & $ exp(-\frac{r}{l}) $\\
	$\gamma$-exponential & $ exp(-\frac{r}{l}^\gamma)$ \\
	rational quadratic & $ (1 + \frac{r^2}{2 \alpha l^2})^{-\alpha} $\\
	neural network &$ sin^{-1} (\frac{2x^T\sum x'}{\sqrt{(1 + 2x^T\sum x)(1 + 2x'^T\sum x')}t})$ \\
	\hline
	\end{tabular}
	\end{center}
	 \caption{Several covariance functions \cite{GPbook}}
	 \label{tab:cov_functions}
\end{table}

We present in the Figures \ref{fig:se}, \ref{fig:matern}, \ref{fig:nn} how changing the covariance function can affect the generated model.

\begin{figure}[h!]
	\centering
		\includegraphics[scale = 0.5]{./img/gp/se.png}
	\caption{Three sample GP functions with squared exponential covariance, lenthscale $ \lambda=5$ and $a=1$ from \cite{ThesisSnelson}}
	\label{fig:se}
\end{figure}

\subparagraph{The squared exponential covariance} from Figure \ref{fig:se}, has unrealisticly strong smoothness assumptions. Nevertheless it is the most used in Machine learning because it is infinitely differentiable. 

\subparagraph{The Matern covariance} from Figure \ref{fig:matern}, is rougher than the squared exponential being considered by some to better map real work circumstances. If we would set the hyperparameter $v \rightarrow \infty $, Matern becomes actually the squared exponential covariance function.
\begin{figure}[h!]
	\centering
		\includegraphics[scale = 0.5]{./img/gp/Matern_cov.png}
	\caption{Three sample GP functions with Matern covariance, $v=1/2$, lenthscale $ \lambda=5$ and $a=1$ from \cite{ThesisSnelson}}
	\label{fig:matern}
\end{figure}


\subparagraph{The neural network covariance} from Figure \ref{fig:nn}, is a nonstationary function.
The prior functions were stationary covariances. Nonstationary covariances allow the model to adapt to functions whose smoothness varies with the inputs. E.g. If the ``noise variance is different in different parts of the input space, or if the function has a discontinuity, a stationary covariance function will not be adequate'' \cite{Nncov}.

\begin{figure}[h!]
	\centering
		\includegraphics[scale = 0.8]{./img/gp/nn.png}
	\caption{Samples GP functions with neural network covariance, $\sigma_0=2$ and  $\sigma$ shown on the image \cite{GPbook}}
	\label{fig:nn}
\end{figure}



\newpage

\subsection*{Advantages}
Using Gaussian Processes has the following advantages:
\begin{itemize}
	\item \textbf{non-parametric}: Successful methods in machine learning are essentially non-parametric.  Moreover, being able to adapt the hyperparameters from the training data and not setting them prior permits a lot of flexibility in unknown datasets. 
	\item \textbf{flexible}: Many popular non-parametric methods used in machine learning can be substituted by particular Gaussian Processes. Flexible models can be generated which have been shown to consistently outperform the more conventional methods.\cite{ThesisRas}
	\item \textbf{unlikely to overfit}: Overfitting, is adapting our model too much on our training data. We end up having a great performance for the training data, but not describing the underlying relationships, therefore obtaining bad predictions on newly seen data. GPs are known to be less prone to overfitting \cite{ThesisPhilip} which should help us create a generalized model from the training data, that could describe well the test data.
	\item \textbf{can measure the probability of our predictions}. We do not only obtain a prediction based on the learned model, but we also estimate the probability of our prediction to be true, based on the model we learned. 
	
\end{itemize}

All these propose GPs to be a valid technique for noise detection, that could achieve good results. A first known disadvantage of GP is the computational complexity. This is $O(n^3)$, $n$=number of datapoints. For our datasets of more than 800 000 instances, it was a problem to use all instances for training. We solved this by training only on a portion of the data(1\%), which made our script execute only two hours. Secondly,  we are expecting GPs not to be able to detect systematic errors, because they would be learned by our model. For removing these, rule based filtering might be more appropriate.

\newpage
\section{Matlab framework}
To build our tool, we used the framework supplied by Rasmussen and Williams \cite{Matlabcode}. It includes several predefined functions to choose from for the mean or covariance, and methods to compute our model and predictions. To define our GP, four choices need to be made:
\begin{itemize}
	\item \textbf{Mean function}: We set this to 0 and learn it from the training data. This is the normally used approach in case no prior information is known about the mean.
	\item \textbf{Covariance function}: There are many possibilities of covariances functions that are predefined in the matlab framework, more can be generated by composing (adding or multiplying) these covariances  or by manually specifying new ones. 
	\item \textbf{Likelihood functions}: ``The likelihood function specifies the probability of the observations given the latent function, i.e. the GP (and the hyperparameters)`` \cite{Matlabcode}
	\item \textbf{The inference methods} ``specify how to compute with the model, i.e. how to infer the (approximate) posterior process, how to find hyperparameters, evaluate the log marginal likelihood and how to make predictions''\cite{Matlabcode}.
\end{itemize}
