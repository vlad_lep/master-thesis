\chapter{Background}

In this chapter we present related literature found on data quality and how Machine Learning was used to improve it. The concepts explained are necessary for appreciating the rest of the thesis.
\begin{center}
\emph{``The decisions made are only as good as the data upon upon which they are based'' \cite{Panda}}
\end{center}


There are many definitions of data quality but the most widely accepted one describes it as ``fitness for purpose'' \cite{DSandDQ}. This implies that in order to consider the quality of a dataset, we need to understand what will it be used for.  
Many datasets are used for research in empirical Software Engineering, for diverse purpose as deciding the quality, predicting the evolution, effort, costs, bugs etc. We believe that the research done in this field stands on the data it uses and ``great care is needed to ensure sufficient attention is paid to the data as well as the algorithms'' \cite{NasaDS}. 


Data quality is seen in literature as a multidimensional concept on which researchers do not have a common agreement \cite{Wang}. Wand and Wang enumerated four dimensions that are repeatedly mentioned in literature \cite{Wang} : 
\begin{itemize}
	\item accuracy
	\item completeness
	\item consistency
	\item timeliness
\end{itemize}


The timeliness dimension raises the issue that historical data might not represent current situations. For example productivity changes over time according to Shepperd \cite{Shepperd05}. Therefore, using old data to make predictions for current productivity could create bad results. 
Completeness, is easy to evaluate even on large datasets.  By creating a script missing measurement can be fast identified.
A consistent dataset presumes that a ``data value can only be expected to be the same for the same situation''\cite{Wang}. 

Accuracy, has no clear definition, incorporating precision of measurements in some cases. We are not interested in precision, as for a given dataset, we can not increase the precision now; this being an early technique to increase data quality. We will follow the definition of Wand and Wang \cite{Wang} which propose that ``inaccuracy implies that information systems represent a real-world state different from the one that should have been represented''. This is much harder to assure as distortions or noise are not easy to discover in large datasets.

\textbf{Noise}, is defined as bad data, an incorrect instance in our dataset. It could have different reasons for appearing: bad data collection, errors in manipulating it or errors in interpreting it. It is certain, that we want to remove noisy instances from our dataset as they can impact our research. This can be achieved by preventive techniques or correcting techniques. In our research, we will focus on the latter.

\textbf{Outliers} are highly atypical values, that appear in rare circumstances. They can also be affected by noise. E.g. If we keep track of activities done each day by people in the USA, "landing on the moon" might be correct for Louis Amstrong but it is certainly not a common activity, yet. Therefore, Louis Amstrong's "landing on the moon" activity would be an outlier in our dataset. On the other hand, if "landing on the moon" is reported for us, as our activity for today, it will be most likely noise.

In Machine Learning some researchers considered outliers to be noise \cite{DSandDQ} since they can create difficulties in research. We consider that depending on the question we want to answer we should exclude  outliers from our data or not. This relates to the definition of quality as "fitness for purpose". 


\section{Noise detection using Machine Learning}
Because of its great impact, data quality is also a concern in other fields and automatic ways to identify noise were tried out using Machine Learning techniques. One broad definition of Machine Learning is the study of algorithms that improves automatically through experience \cite{M97}.  
The typical approach for noise detection is to learn some classifier based on the data, predict the labels of the points and consider wrongly classified points as suspect.
In this section, we will present some automated techniques, used by researchers in different fields for noise identification.

In medicine, many diagnostics are made based on data, making the quality an important factor. Different techniques have been investigated to detect noise in data and correct it. Gamberger  uses compression based induction to handle noise \cite{MedicalNoise}. Their idea is founded on the MDL (Minimum Description Length), which proposes that the best hypothesis for a given dataset is the one that lead to the best compression of the data. By cleaning the dataset in this way, they managed to improve the classification for early diagnosis of rheumatic diseases. Similar techniques have been tried with success by Gamberger and colleagues for detecting coronary artery disease in \cite{MedicalNoise2}. 

MLD is a formalization of Occam's Razor, which states that among all models that correctly fit the training data, we should select the simplest. In the same category of algorithms decision trees were built in \cite{SaturationFilters} which were simplified by pruning techniques. PCA (Principle component analysis), a dimensionality reduction algorithm could also be used.

Brodley and Friedl \cite{Brodley} focus on datasets used for automated land-cover mapping from satellite data, credit approval, scene segmentation, road segmentation and fire danger prediction. They used a different approach, by splitting their dataset into $n$ parts. They created $n$ models, with $n-1$ segments of data, and used the remaining piece for testing. Contrary to the previously described algorithms, in this case, the training and testing data are kept separately; we learn the model on one part and we make our prediction on the remaining. Moreover, in \cite{Brodley}, they introduced the idea of multiple base classifiers. Basically they used decision trees, nearest neighbor and a linear machine algorithm  as base classifiers to separately model the data and make classification. Afterwards, each classifier votes for each point, if it is noisy or not. 

Finally, another class of techniques to deal with noise, is data polishing \cite{LiebchenThesis}. These methods detect noise but instead of removing it, they correct the erroneous values. For small collections, removing noise from the dataset decreases the number of instances, making polishing an interesting technique. We do not consider this is of interest in the case of large datasets. In this case it is better to remove noise, not to create artificial points in our data. 

In this thesis, in order to detect noise we use Gaussian Processes, a probabilistic approach. They are based on different mathematical concepts than the previously mentioned techniques. Also, the problem that we approach on the Ohloh dataset, is different from the aforementioned ones since we do not have labeled data or a secondary framework that we could test our resultson; as Gamberger had a tool that predicted coronary artery disease. They improved its performance by cleaning the data. Nevertheless, we can notice some similarities to the other techniques. From some perspective, the way we use Gaussian processes, could be seen similar to a data compression technique since we learn a model that approximates the data. Second, we can keep training and test data separately and we can generate more models on different parts of the data. Last, since Gaussian Processes make predictions, we can polish our data by replacing the measured values with our prediction for the instances we consider noise.

\newpage
\section{Data quality addresses in Software Engineering}

In empirical Software Engineering the main efforts are done by Khoshgoftaar and colleagues \cite{Nasa1, Nasa2, Panda}, Liebchen and Shepperd \cite{DSandDQ, LiebchenThesis, LibAll}. Liebchen in his thesis \cite{LiebchenThesis} researched how empirical analysts address the problem of data quality. He found that out of 161 studies:

\begin{enumerate}

\item \textbf{50 studies} (31\%) Avoid poor data by improving the \textit{data collection} procedures. This is a noise prevention technique, which is not always possible since many times we analyse data sets that were collected somewhere in the past.
\item \textbf{35 studies} (22\%) \textit{Manually check} the quality of the data. This is done by inspecting the measurements, measuring the same attributes in different ways and comparing the results, visual inspection.
\item \textbf{18 studies }(11\%) \textit{Use meta-data}, for example to describe the perceived level of quality for each attribute or instance. This is commonly used by in the ISBSG benchmark data sets, which assign grades from A(highest quality) to D(lowest qulity) to their measurements. \cite{ISBSG}. Their grades describe the completeness of the data, having no missing values, which is not equal to ensuring quality. 
\item \textbf{16 studies} (10\%) Use \textit{automated noise detection techniques} to improve their data quality 
\item \textbf{21 papers} (13\%) Carried out an \textit{empirical analysis of noise}.

\end{enumerate}

A comprehensive list with descriptions of used algorithms is found in Chapter 2 and 3 of Liebchen's PHD thesis \cite{LiebchenThesis}. We will briefly summarize some of the used techniques in this section.

Khoshgoftaar propose noise detection techniques based on boolean rules generated from the data \cite{Nasa1}. They test their research by artificially injecting noise in NASA datasets, obtaining good results.

Secondly, they proposes the Pairwise Attribute Noise Detection Algorithm or PANDA. PANDA analyses each two attribute pairs from the dataset and calculates the mean and variance base on all the records for these two attributes. Then, for each instance, a score is assigned that describes its probability to be noise. Large deviations from normal, have higher contributions to the score. The final score for each instance is computed based on all the scores obtained for each combination of two attributes. 
The authors showed that PANDA found more noisy instances, fewer outliers and fewer clean instances than the nearest neighbor outlier detection technique.

Liebchen used in his thesis \cite{LiebchenThesis} three types of decision tree methods to handle noise: Robust Filtering, Predictive Filtering and Filter and Polish. He validates his approach on real data sets, using domain experts to decide on the correctness of their classification. Their results are good, but when testing the algorithm on simulated data the results are less promising. This shows how hard it is to create a solution that can handle different types of noise, in different domains. 

More efforts to maintain high quality datasets in the empirical software engineering field have been made by the PROMISE Group which currently has 20 data sets. 

Most proposed techniques are evaluated by injecting artificial noise in datasets. The first limitation of this is that we do not know if the data that we use and inject noise into, does not have noise already. Second, the noise is usually added randomly with a distribution, but this might not accurately simulate real world noise.

This thesis examines the use of Gaussian Processes for noise detection on software engineering datasets. We test our approach on simulated noisy datasets but also on real world data with noise. We believe Gaussian Processes could perform well in detecting noise, since they create flexible models and are non-parametric approaches. Therefore, we do not need to set ourselves parameters that describe our data, they are learned in the training phase. This is a big advantage for datasets of which we have no prior knowledge. From what we know, Gaussian Processes have not been used until now for noise detection, this implies a beginning risk for us. Also their computational complexity is $O(^3)$ , which could create problems for our big datasets from Ohloh.

