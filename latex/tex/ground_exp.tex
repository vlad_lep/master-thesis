\chapter{Simulated noise experiment} 
Before running the GP algorithm on the unknown Ohloh data, we measure its potential in identifying noise in controlled settings. By this we test:
\begin{itemize}
	\item if Gaussian processes are capable to detect noise 
	\item if our methodology, our way of setting up the experiment, is correct and
	\item experiment with different GP settings and see their influence on the results
\end{itemize}

As a ground truth experiment we chose a known dataset, added artificial noise to it and measured the noise detection performance of our approach. The following sections describe the chosen dataset, the experimental setup, the results obtained and states our conclusions. 

\section{Dataset description}
In order to determine if Gaussian Processes is a viable algorithm for our problem, we first selected a known database for experimenting. We chose from the UCI repository the Housing dataset. It contains 506 instances, each of them describes 14 characteristics of housing in the suburbs of Boston. The description of all the attributes can be seen in  Table \ref{tab:housing_attr}.

\begin{table}[h!]
  \begin{center}
  \resizebox{\columnwidth}{!}{%
	\begin{tabular}{ | l |l | }
	  \hline
	  Abbreviation & Description  \\
	  \hline
	CRIM & per capita crime rate by town \\
	ZN & proportion of residential land zoned for lots over 25,000 sq.ft. \\
	INDUS & proportion of non-retail business acres per town \\
	CHAS & Charles River dummy variable (= 1 if tract bounds river; 0 otherwise) \\
	NOX &	nitric oxides concentration (parts per 10 million) \\
	RM & average number of rooms per dwelling \\
	AGE & proportion of owner-occupied units built prior to 1940 \\
	DIS & weighted distances to five Boston employment centres \\
	RAD & index of accessibility to radial highways \\
	TAX & full-value property-tax rate per \$10,000 \\
	PTRATIO & pupil-teacher ratio by town \\
	B & $1000(Bk - 0.63)^2$ where Bk is the proportion of blacks by town \\
	LSTAT & \% lower status of the population\\
	MEDV  & Median value of owner-occupied homes in \$1000's \\
	\hline
	\end{tabular}
	}
	\end{center}
	 \caption{Housing attributes }
	 \label{tab:housing_attr}
\end{table}

We considered this dataset an appropriate one for our experiment since it is highly used in the Machine Learning community, increasing our trust in the validity of the data. Moreover it has similarities with Ohloh :
\begin{itemize}
  \item it is positively defined and has no missing values for attributes 
  \item contains 13 continuous attributes (including "class" attribute "MEDV") and 1 binary-valued attribute \cite{HousingDS}. This is ideal for regression and similar to Ohloh where all attributes are continuous.
  \item it is complex enough not to represent a trivial test for GP. The number of features is higher than for our Ohloh dataset and their correlation is diverse as shown in the Figures \ref{fig:crim_indus} \ref{fig:dis_vs_rad}, \ref{fig:medv_crim}, \ref{fig:zn_medv}.
\end{itemize}


\begin{figure}[h!]
	\centering
    \begin{minipage}[b]{.35\textwidth}
    	\centering
        \includegraphics[width=\textwidth]{./img/ground/DIS_VS_RAD.png}
        \caption{Correlation between DIS and RAD}
        \label{fig:dis_vs_rad}
    \end{minipage}
	\qquad
    \begin{minipage}[b]{.35\textwidth}
    	\centering
        \includegraphics[width=\textwidth]{./img/ground/ZN_vs_medv.png}
        \caption{Correlation between ZN and MEDV}
        \label{fig:zn_medv}
    \end{minipage}
    \qquad
        
    \begin{minipage}[b]{.35\textwidth}
        \includegraphics[width=\textwidth]{./img/ground/CRIM_vs_INDUS.png}
        \caption{Correlation between CRIM and INDUS}
        \label{fig:crim_indus}
    \end{minipage}
       \qquad 
    \begin{minipage}[b]{.35\textwidth}
        \includegraphics[width=\textwidth]{./img/ground/CRIM_vs_medv.png}
        \caption{Correlation between MEDV and CRIM}
        \label{fig:medv_crim}
    \end{minipage}
    \qquad
\end{figure}

\clearpage

\section{Experimental setup}
For our experiment, we considered the first 13 dimensions as input features, while MEDV is the attribute that we want to predict. Our objective is to answer the question:

\begin{center}
\emph{"If we modify the predicted MEDV value of the house, for some instances, between -100\% and +100\%, can we detect the noisy points in our data using Gaussian processes? "}
\end{center}

New points with noise were created by randomly sampling 50 instances from the existing housing dataset and afterwards adding random noise to them. Adding noise presumes, in our case, modifying the MEDV values by randomly multiplying them with a coefficient between -1 and 1, while keeping the other features intact. This is equivalent to saying, that in case all the other properties of an instance remain the same, the predicted median value of the home will increase or decrease with max 100\%. 


The distribution of the used noise is presented below. This distribution has impact on our experiment, since adding 2\% to MEDV is much harder to detect as noise (maybe it should not even be considered noise) than adding or subtracting 90\% of the original MEDV, which should be definitely highlighted.

\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth]{./img/ground/noise_percentage_histogram.png}
	\caption{Histogram of percentage of noise added to MEDV}
	\label{fig:hist_medv}
\end{figure}

After creating the noisy points, the dataset was randomly split in two parts. We obtained 259 data points for training and 247 for testing. To the testing dataset we added the 50 new noisy points, ending up to a total of 297. 

\newpage
Finally, we used the GPs regression algorithm on our data. We aimed to test three characteristics:
\begin{enumerate}
\item{The success of GP in the default settings}
\item{What is the impact on the results when varying the hyperparameters?}
\item{How does changing the covariance function impact the results?}
\end{enumerate}
Our results are discussed in the following section.

\section{Results}
\subsection{Gaussian processes in the default settings}
To show the capabilities of the GP algorithm, several experiments were run on the housing dataset. In the default GP process, we chose the following configuration :
\begin{itemize}
\item{\textbf{mean function}: it is set to zero and learned from the training data.}
\item{\textbf{covariance function}: We used the neural network covariance function with the posterior hyperparameters learned from the training data.}
\item{\textbf{likelihood function}: The Gaussian likelihood with posterior standard deviation learned from the training data.}
\item{\textbf{the inference method} is exact, without approximations.}
\item{\textbf{maximum number of function evaluations} is set to 100.}
\end{itemize}

Using this trained model, we calculated the log-probabilities of each of the points from the test dataset(including the new noisy instances) and computed the Receiver operating characteristic(ROC)curve. This captures both the true positive rate (TPR), the noisy points correctly classified, and the false positive rate(FPR), the percentage of normal points classified as noisy of the model when the discriminating  threshold is varied.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.6\textwidth]{./img/ground/ROC_all_noise.png}
	\caption{ROC curve for GP algorithm on all the test dataset.}
	\label{fig:ROC_all_noise}
\end{figure}

Figure \ref{fig:ROC_all_noise} shows how selecting different thresholds (minimum accepted probabilities) we can detect the noise in the test dataset and the effect it has on the FPR(normal instances wrongly classified as noise).  We notice that we can find 54\% of the added noise with less than 9\% FPR. 

By sorting the log probabilities of points to be noise, in the first 50 most probable we had 27 actual noise points. These results show a very good performance of the algorithm for the current dataset. Our approach does not make the difference between outlier and noise. Therefore we should consider that in the first 50 most probable points, besides the 27 noisy ones, we could have also highlighted outliers in fact, which could be of interest in some cases.

By manually inspecting the log-probabilities assigned to each point, we noticed that the first 12 most probable points are labeled correctly as noise. On the other hand, the last 4 points, that should have the lowesst probability to be noise, are actually noisy points induced by us. We can explain this by recalling the noise distribution from Figure \ref{fig:ROC_all_noise}. From the histogram we can see that the added noise can be between -100\% to 100\%. It is clear that bigger changes make it easier for us to identify noisy points. In our case the last 4 points were changed with approximately 2\%, 12\%, 4.7\% and  4\%. 

Nevertheless, this should not make them more probable than points that have not been changed at all. The complete explanation lies in the way we generated noise. We randomly selected 50 points from all our data and created new instances with noise from them. Thus some of the original instances were split in training others in test data. The original measurements for these 4 data points ended up in the training dataset and the modeled managed to fit them very well. This, correlated with the small amount of noise added, made the disturbed instances from the the test data hard to distinguish. 

Our manual analysis highlighted two concerns. First, small changes are impossible to detect and might not be noise, as our inputs are continuous and slight variation in value estimation is normal. Second, if the noise is similar to the training data (or part of the training data, as it will be in the Ohloh dataset) its probability to be classified as noise will decrease. 

We ran more tests on the dataset, to measure how would our performance change if we remove the noisy instances, from the test data, that changed the initial measurement with less than 20\% respectively 40\%.
\begin{figure}[h!]
	\centering
    \begin{minipage}[b]{.35\textwidth}
    	\centering
        \includegraphics[width=\textwidth]{./img/ground/ROC_minus_below_20_noise.png}
    \end{minipage}
	\qquad
    \begin{minipage}[b]{.35\textwidth}
    	\centering
        \includegraphics[width=\textwidth]{./img/ground/ROC_minus_below_40_noise.png}
    \end{minipage}
    \qquad
    \caption{ROC after removing noise points that affect MEDV with less than 20\% (left) and 40\% (right)}
    \label{fig:roc_rem_noise}
\end{figure}

The ROC obtained after removing less clear noise shows a visible increase in performance. Also in Table \ref{tab:compare_precision} we compare the difference in performance based on the given noise. The decrease in FPR is significant when the noise is more significant.

\begin{table}[h!]
  \begin{center}
	\begin{tabular}{ | p{5cm} |p{2cm} |p{2cm} | p{4cm} | }
	  \hline
	  {\bfseries Test data with} & {\bfseries Remaining noise} & {\bfseries FPR for 50\% recall} & {\bfseries Number of instances found in the first 50}  \\
	  \hline 
	  All the noise & 50 & 9\% & 27 \\
	   \hline
  	  Noise with more than 20\% deviation from original & 39 & 4.4 \% & 26 (22 out of first 30) \\
  	   \hline
  	  Noise with more than 40\% deviation from original & 30 & 1.2 \% & 25 (19 out of first 30) \\
	  \hline
	\end{tabular}
	\end{center}
	\caption{Comparison between the 3 test data containing different amount of noise}
\label{tab:compare_precision}
\end{table}

\subsection{Impact of the hyperparameters}
The posterior hyperparameters of the Gaussian Process are learned from the training data. Nevertheless, the number of function evaluations is manually set by us. This parameter sets the maximum number of evaluations preformed, if convergence is not reached before. We tested how varying it can influence our performance. 

We realized that the performance of the algorithm can be significantly changed as noticed by the results in Figure \ref{fig:max_f_var}. In these figures we tested the performance of each of the models created on the three types of testing data (all instances, or instances with noise variance higher than 20\% and 40\% from normal) by varying the number of function evaluations from 1 to 100. We measured the FPR to detect A minimum 50\% of the noisy points. We are aiming to obtain the smallest values for the FPR.

\begin{figure}[h!]
	\centering
    \begin{minipage}[b]{.45\textwidth}
    	\centering
        \includegraphics[width=\textwidth]{./img/ground/max_functions_all.png}
    \end{minipage}
	\qquad
    \begin{minipage}[b]{.45\textwidth}
    	\centering
        \includegraphics[width=\textwidth]{./img/ground/max_functions_20.png}
    \end{minipage}
    \qquad
    \begin{minipage}[b]{.45\textwidth}
    	\centering
        \includegraphics[width=\textwidth]{./img/ground/max_functions_40.png}
    \end{minipage}
    \qquad
    \label{fig:max_fcts}
    \caption{FPR to detect minimum 50\% of the noise on the 3 test datasets when varying the maximum number of function points}
\label{fig:max_f_var}
\end{figure}

The best results are obtained when the parameter is set to 12. In this case the FPR becomes 4.8\% for the test data with all the noisy points, 0.8\% and 0\% for test data with more than 20\% respectively 40\% variance from normal.

The performance can be greatly changed when setting the parameter to 13. On the test data with all the points, the FPT reaches 100\%, meaning that we can not detect 50\% of the noise without looking at all the other normal inputs from the testing dataset. This is highly undesirable. 

The negative log probability likelihood obtained for the different number of function evaluations is plotted in Figure \ref{fig:nlz_for all noise}. We want to minimise this. A smaller value, states that the obtained model is more likely to correctly approximate the training data. We notice that 15 function evaluations is a cutting point, after which our approximation of the training data becomes better. Therefore convergence is reached on this dataset by setting the maximum number of function evaluations higher than 15. It is safer, in general, to choose a substantially higher number. 

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.7\textwidth]{./img/ground/nlz_for_all_noise.png}
	\caption{Variation of the  negative log probability likelihood with the maximum number of function evaluations}
	\label{fig:nlz_for all noise}
\end{figure}


We conclude that to minimize the problem, it is safer to choose a higher number of basis functions and have a stable prediction, instead of working in the “risky area”.

\newpage
\subsection{Impact of the  covariance function}
\subsubsection*{The Matern covariance}
We tested how changing the covariance function would affect our performance. Based on our data, we considered that the Matern covariance function would be another suitable option. In Figure \ref{fig:matern_ROC} we plotted the ROC curve. 

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.6\textwidth]{./img/ground/cov/cov_mat_d3.png}
	\caption{ROC for Matern covariance function}
	\label{fig:matern_ROC}
\end{figure}

The differences between the ROCs of these functions are not that high, nevertheless the area below the curve is higher for the Matern covariance than of the model obtained with the neural network function. On the other hand, looking at the beginning of the curve we notice that the FPR has a increasing “hop” around 20\% recall, while for neural networks it is around 30\%. This would mean that the first 20\% of noise instances can be noticed with a small FPR for the Matern covariance function while for Neural Network we can detect 30\% before increasing the FPR, making it more convenient for big datasets. If we would aim to detect a higher percentage of noisy points the Matern covariance function would be more appropriate. 

\subsection{Varying other settings}
\subsubsection*{Using the Inverse Gaussian likelihood function for strictly positive data and the Laplace approximation}
To force our prediction to be in the positive domain we can change the likelihood function and the inference method. This combination generated the ROC from Figure \ref{fig:lik_InvGauss} on all the dataset. 

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.6\textwidth]{./img/ground/lik_InvGauss.png}
	\caption{ROC for the Inverse Gaussian likelihood function}
	\label{fig:lik_InvGauss}
\end{figure}

The result is noticeably better than the previous inference method, creating an increased steep at the beginning of the curve, meaning that more noise can be detected with less false positives. This is confirmed by our second measurement; we detected 50\% of the noise with 1.21\% FPR instead of 9\%. On the other hand, when usingh the Inverse Gaussian likelihood, the domain of the output is forced to be strictly higher than zero, which is not the case for our Ohloh dataset where we have zero values.

\subsubsection*{Using approximation methods for big data sets}
The computational complexity of the normal GP is \(O(n^3)\) where n is the number of data points. Approximation methods have been developed that lower the computational complexity to \(O(n^2*m)\) where m is a smaller parameter set by the approximation method. We tried these methods on our data and obtained the ROC in Figure \ref{fig:aprox_GP}. 
\begin{figure}[h!]
	\centering
	\includegraphics[width=0.6\textwidth]{./img/ground/approximation_methods.png}
	\caption{ROC for approximation methods}
	\label{fig:aprox_GP}
\end{figure}
As expected the accuracy of our noise detection decreases significantly. This has also been shown in other research \cite{aproxGP}, where a full GP has better performance than approximation methods. Based on these, we will use the full GP for Ohloh, which can support up to 10 k points.

\section{Discussion}
Our ground truth experiment \textbf{confirmed that GP can be successfully used to detect artificially induced noise }in the data with good accuracy and that our experimental setup is valid, but also highlighted several challenges that we will have to confront on the Ohloh dataset:
\begin{itemize}
\item{\textbf{False positive rate (FPR)} becomes more important in big datasets. Putting it in perspective, for the Ohloh dataset we have more than 800 000 points. A 9\% FPR would mean that around 82 000 points are wrongly classified, which is a big amount of wrongly labeled data.}
\item{\textbf{If we train our model on noisy points, these points might become harder to detect afterwards}. In the Ohloh dataset we do not have the luxury of knowing what points are noise and exclude them from the training dataset.}
\item{\textbf{Outliers and noise can not be differentiated}, since we do not have labeled data. The instances that we highlight need to be manually inspected in order to conclude if they are noise, outliers or false positives.}
\item{\textbf{The performance is influenced by the noise type}. Our approach is influence by the noise amount (deviation from normal). Small noise is very hard to detect, while big variations from the normal case will be easier to detect.}
\item{\textbf{The maximum number of functions evaluated influences the performance of the GP algorithm.} A high number should be chosen; this would let the inference converge by itself and not stop it prematurely.} 
\item{\textbf{Using likelihoods function for strictly positive data can increase our performance} also in Ohloh. On the other hand our outputs can have zero as a value, are not strictly positive, which means it would not be able to calculate the probabilities for these points. We could adapt all zeros to be a bit higher than zero but this would presume altering much of the data. }
\end{itemize}