function [ FPR ] = fpr_for_recall_above(  roc_results ,threshold )
%FPR_FOR_RECALL_ABOVE Calculate what is the minimum False Positive Rate(FPR) for a
%roc to have a probability above the threshold
FPR =0;
    for i=1:size(roc_results,1)
        if roc_results(i,2)>=threshold
            FPR = roc_results(i,1);
            break;
        end
    end


end

