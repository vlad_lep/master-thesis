function [ train ] = split_data( all_data, percentage_train )
%SPLIT_DATA Split all_data in 2 parts, for training and testing.
probability = rand(size(all_data,1),1);
probability_train  = percentage_train /100;
size_train = 1;
for i=1:size(all_data,1)
    if probability(i) < probability_train
        train(size_train,:) = all_data(i,:);
        size_train = size_train + 1;
    end
end


end

