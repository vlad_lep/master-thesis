
x_size = size(activity_train);
test_size = 100;
x= activity_train(:,2:5);
y =activity_train(:,10);
z = activity_array(:,2:5);
zs = activity_array(:,10);

max_functions = -100;
L = 1; %
sf =2; %signal variance

covfunc = {'covNNone'}; hyp.cov = log([L;sf]);
likfunc = @likGauss; 
sn = 0.1; % noise variance
hyp.lik = log(sn);

inference = @infExact;
clock
hyp = minimize(hyp, @gp, -max_functions, inference, [], covfunc, likfunc, x, y);
clock
[m_activity_sel, s2, fmu, fs2, lp_activity_sel] = gp(hyp, inference, [], covfunc, likfunc, x, y, z, zs);
clock

results_activity_selection(:,:) = [activity_array(:,:) lp_activity_sel(:,1) m_activity_sel(:,1)];
results_activity_selection = sortrows(results_activity_selection, 11);


