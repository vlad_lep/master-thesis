%COMPARE_GPS Compare GP models (all noise, above 20 and above 40 based on
%the FPR and generate graphs.

close all;
clear all;
load ('housing_exp/housing.mat');

x= train_data(:, 1:13);
y =train_data(:,14);
z = test_data(:, 1:13);
zs = test_data(:,14);
noise_test = noise_data(:,1:13);
noise_test_label = noise_data(:,14);
L = 1; %number of points
sf =2; %signal variance

covfunc = {'covNNone'}; hyp.cov = log([L;sf]);
likfunc = @likGauss; 
sn = 0.1; % noise variance
hyp.lik = log(sn);
FPR_1 = zeros(100,1);
FPR_2 = zeros(100,1);
FPR_3 = zeros(100,1);
for max_functions = 1:100
    hyp = minimize(hyp, @gp, -max_functions, @infExact, [], covfunc, likfunc, x, y);
    nlz = gp(hyp, @infExact, [], covfunc, likfunc, x, y);
    [m, s2, fmu, fs2, lp] = gp(hyp, @infExact, [], covfunc, likfunc, x, y, z, zs);
    [m, s2, fmu, fs2, lpnoise] = gp(hyp, @infExact, [], covfunc, likfunc, x, y, noise_test, noise_test_label);

    lp(:,2) = 1:size(lp,1);
    lp(:,3) = -1; % correct point
    lpnoise(:,2) = 1:size(lpnoise,1);
    lpnoise(:,3) = 1; %noisy point
    final_lp = [lp ; lpnoise];
    roc_results = calc_ROC_percentages( final_lp(:,1), final_lp(:,3), size(lpnoise,1));
    FPR_1(max_functions) = fpr_for_recall_above(roc_results,0.5);
%     FPR_2(max_functions) = nlz    ;
    
    new_lp = remove_noise_bellow_threshold(lpnoise, weights, 0.20);
    final_lp = [lp ; new_lp];
    roc_results = calc_ROC_percentages( final_lp(:,1), final_lp(:,3), size(new_lp,1));
    FPR_2(max_functions) = fpr_for_recall_above(roc_results,0.5);


    new_lp = remove_noise_bellow_threshold(lpnoise, weights, 0.40);
    final_lp = [lp ; new_lp];
    roc_results = calc_ROC_percentages( final_lp(:,1), final_lp(:,3), size(new_lp,1));
    FPR_3(max_functions) = fpr_for_recall_above(roc_results,0.5);
end
figure(1)
plot(FPR_1);
xlabel('Max number of function evaluations');
ylabel('FPR to detect minimu 50% of noise');
title('Test data with all noisy points');

figure(2)
plot(FPR_2);
xlabel('Max number of function evaluations')
ylabel('FPR to detect minimu 50% of noise');
title('Test data with noise varying more than 20% ');

figure(3)
plot(FPR_3);
xlabel('Max number of function evaluations');
ylabel('FPR to detect minimu 50% of noise');
title('Test data with noise varying more than 40% ');



