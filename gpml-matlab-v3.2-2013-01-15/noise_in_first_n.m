function [ no_noise ] = noise_in_first_n( labels, n )
%noise_in_first_n Calculates the number of noisy points in the first n
%   Detailed explanation goes hereTODO: nlml2 understand this result for estimating the GP model
no_noise=0;
for i=1:n
    if labels(i)==1
        no_noise = no_noise +1;
    end
end

end

