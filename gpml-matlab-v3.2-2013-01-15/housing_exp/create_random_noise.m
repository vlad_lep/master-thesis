noise =[];

no_noise = 50; % 10% of the data
weights = (rand(50,1)-0.5) *2;

selected_datapoints = randi(506,50,1);
noise_data = housing(selected_datapoints,:);
noise_data(:,14) = noise_data(:,14) + noise_data(:,14).* weights;