function [ train_data test_data ] = split_train_test_data( all_data )
%SPLIT_TRAIN_TEST_DATA Summary of this function goes here
%   Detailed explanation goes here
prob = rand(size(all_data),1);
train_data = [];
test_data = [];
tr_cont = 1;
tst_cont = 1;
for i=1:size(all_data,1)
    if prob(i)>0.5
        train_data(tr_cont, : ) = all_data(i,:);
        tr_cont = tr_cont+1;
    else
        test_data(tst_cont,:) = all_data(i,:);
        tst_cont = tst_cont+1;
    end
end


end

