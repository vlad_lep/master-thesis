close all;
clear all;

load housing.mat
figure(1);
scatter(housing(:,8), housing(:,9));
ylabel('RAD', 'fontsize',16);
xlabel('DIS', 'fontsize',16);

figure(2);
scatter(housing(:,1), housing(:,14));
ylabel('MEDV', 'fontsize',16);
xlabel('CRIM', 'fontsize',16);

figure(3);
scatter(housing(:,2), housing(:,14));
xlabel('ZN', 'fontsize',16);
ylabel('MEDV', 'fontsize',16);

figure(4);
scatter(housing(:,1), housing(:,3))
xlabel('CRIM','fontsize',16);
ylabel('INDUS', 'fontsize',16);

figure(5);
hist (weights*100, 50);
xlabel(' +/- percentage of noise added');
ylabel(' number of points')

figure(6);
scatter(housing(:,1), housing(:,14));
hold all;
scatter(noise_data(:,1), noise_data(:,14));
ylabel('MEDV', 'fontsize',16);
xlabel('CRIM', 'fontsize',16);




plot([1:10],argoResults,'--gs','LineWidth',2,...
                'MarkerEdgeColor','g','MarkerFaceColor','g','MarkerSize',5);
text(2.25,argoResults(2),'\leftarrow Argo UML' ,'HorizontalAlignment','left','Color','g')           


function [squareError,percentageResults] = calcError( x1,x2, data, prjName )
    error= 0;
    maxDiffLOC =10;
    percentageResults = [];
    for diffLOC =1:maxDiffLOC 
    percentCorrect =0;
     for i=1:size(data,1)
            error = error + (x1 *data(i,1) + x2 - data(i,2))^2;
                if ((x1 *data(i,1) + x2 - data(i,2))^2 <diffLOC^2)
                    percentCorrect = percentCorrect +1;
                end
        end
        squareError = error / size(data,1);
        percentCorrect = percentCorrect / size(data,1) * 100;
    percentageResults = [percentageResults ;percentCorrect];
    end
    plot([1:maxDiffLOC],percentageResults);
    percentageResults
    xlabel('Difference LOC accepted');
    ylabel('Correct percentage prediction');
    print('-dpng', strcat(prjName,'-Success percentage'));
end

