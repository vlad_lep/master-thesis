%test how would Rascal predict itself
x_size = size(activity_train);
test_size = 100;
x= rascal_activity(:,2:9);
y = rascal_activity(:,10);
z = rascal_activity(:,2:9);
zs = rascal_activity(:,10);

max_functions = -100;
L = 1; %
sf =2; %signal variance

covfunc = {'covNNone'}; hyp.cov = log([L;sf]);
likfunc = @likGauss; 
sn = 0.1; % noise variance
hyp.lik = log(sn);

inference = @infExact;
clock
hyp = minimize(hyp, @gp, -max_functions, inference, [], covfunc, likfunc, x, y);
clock
[m_rascal, s2, fmu, fs2, lp_rascal] = gp(hyp, inference, [], covfunc, likfunc, x, y, z, zs);
clock

results_rascal(:,:) = [rascal_activity(:,:) lp_rascal(:,1) m_rascal(:,1)];
results_rascal = sortrows(results_rascal, 11);


