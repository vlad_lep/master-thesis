

x = size_train(:,2:7);
y =size_train(:,8);
z = size_array(:,2:7);
zs = size_array(:,8);

max_functions = -100;
L = 1; %
sf =2; %signal variance
 
covfunc = {'covNNone'}; hyp.cov = log([L;sf]);
% covfunc = {@covMaterniso, 3}; ell = 1; hyp.cov = log([ell; sf]);
likfunc = @likGauss; 
sn = 0.1; % noise variance
hyp.lik = log(sn);

inference = @infExact;
clock
hyp = minimize(hyp, @gp, -max_functions, inference, [], covfunc, likfunc, x, y);
clock
[m_size, s2, fmu, fs2, lp_size] = gp(hyp, inference, [], covfunc, likfunc, x, y, z, zs);
clock

results_size(:,:) = [ size_array(:,:) lp_size(:,1) m_size(:,1)];
results_size = sortrows(results_size, 9);


