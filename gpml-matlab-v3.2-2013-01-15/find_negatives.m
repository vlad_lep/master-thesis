function [ index projects] = find_negatives( results, facts)
%FIND_NEGATIVES Summary of this function goes here
%   Detailed explanation goes here
    index = [];
    projects = [];
    for i=1:size(results,1)
        
        if (sum(results(i,:)<0) >0)
            
            projects = [projects, results(i,1)];
            index = [index; i];  
        end
        
    end
    projects = unique(projects);

end

