% 7973 - rascal id projectx
x_size = size(activity_train);
test_size = 100;
x= activity_train(:,2:9);
y =activity_train(:,10);
z = activity_array(:,2:9);
zs = activity_array(:,10);

max_functions = -100;
L = 1; %
sf =2; %signal variance

covfunc = {'covNNone'}; hyp.cov = log([L;sf]);
likfunc = @likGauss; 
sn = 0.1; % noise variance
hyp.lik = log(sn);

inference = @infExact;
clock
hyp = minimize(hyp, @gp, -max_functions, inference, [], covfunc, likfunc, x, y);
clock
[m_activity, s2, fmu, fs2, lp_activity] = gp(hyp, inference, [], covfunc, likfunc, x, y, z, zs);
clock

results_activity(:,:) = [activity_array(:,:) lp_activity(:,1) m_activity(:,1)];
results_activity = sortrows(results_activity, 11);


