function [] = find_strange_projects(size_f, activity_f)
%Find projets that have a different size for activity and size facts, and
%are not all 0
for i=1:size(size_f,2)
    if size(size_f(i).code,1) >1 
        if i>201
            start =i-100;
        else
            start = 1;
        end
        for j=start:size(activity_f,1)
            if strcmp(size_f(i).name, activity_f(j).name)
                if size(activity_f(j).code_added,1) ~= size(size_f(i).code,1)
                    size_f(i).name
                end
            end
        end
    end
end