    function [ output_data ] = struct_to_array_size( activity)
%STRUCT_TO_ARRAY_ACTIVITY Summary of this function goes here
%   Detailed explanation goes here
output_data= [];
contor = 1;
for i=1:size(activity,2)
    for j=1:size(activity(i).code,1)
        output_data(contor,:) = [i j activity(i).code(j) activity(i).comments(j) activity(i).blanks(j) activity(i).comment_ratio(j) activity(i).man_months(j) activity(i).commits(j) ];
        contor = contor+1;
    end
    if mod(i,200) ==0
        i
    end
end
end

