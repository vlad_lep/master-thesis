% clear all, close all
% load all_data_with_errors_v3;

x = in_data(:,1);
y = in_data(:,2);    
z = [0:142]';
TODO use likGamma - for + data
%model 1: sum mean, materniso cov, gauss likelihood
% meanfunc = {@meanSum, {@meanLinear, @meanConst}}; hyp.mean = [0.5; mean(x)];
% covfunc = {@covMaterniso, 3}; ell = 1/4; sf = 1; hyp.cov = log([ell; sf]);
% likfunc = @likGauss; sn = 0.1; hyp.lik = log(sn);
% nlml2 = gp(hyp, @infExact,  meanfunc, covfunc, likfunc, x, y)
% [m s2 fmu, fs2, lp] = gp(hyp, @infExact, meanfunc, covfunc, likfunc, x, y, z)


% model 2: Calculate the mean and covariance hyp from training covSEiso
% covfunc = @covSEiso; hyp2.cov = [0; 0]; hyp2.lik = log(0.1);
% likfunc = @likGauss; sn = 0.1; hyp.lik = log(sn);
% hyp2 = minimize(hyp2, @gp, -100, @infExact, [], covfunc, likfunc, x, y);
% exp(hyp2.lik)
% nlml2 = gp(hyp2, @infExact, [], covfunc, likfunc, x, y)
% [m s2] = gp(hyp2, @infExact, [], covfunc, likfunc, x, y, z)

% figure(1)
% f = [m+2*sqrt(s2); flipdim(m-2*sqrt(s2),1)];
% fill([z; flipdim(z,1)], f, [7 7 7]/8);
% hold on; plot(z, m, 'LineWidth', 2); plot(x, y, '+', 'MarkerSize', 12)


% model 3: 
% meanfunc = {@meanSum, {@meanLinear, @meanConst}}; hyp.mean = [0.5; mean(x)];
% covfunc = @covSEiso; 
% likfunc = @likGauss; sn = 0.1; hyp.lik = log(sn);
% hyp.cov = [0; 0]; hyp.mean = [0; 0]; hyp.lik = log(0.1);
% hyp = minimize(hyp, @gp, -100, @infExact, meanfunc, covfunc, likfunc, x, y);
% nlml2 = gp(hyp, @infExact, meanfunc, covfunc, likfunc, x, y)
% [m s2 lmu ls2 lp] = gp(hyp, @infExact, meanfunc, covfunc, likfunc, x, y, z)
% 
% f = [m+2*sqrt(s2); flipdim(m-2*sqrt(s2),1)];
% fill([z; flipdim(z,1)], f, [7 7 7]/8)
% hold on; plot(z, m); plot(x, y, '+');


%model 4: 'large scale regression using the FITC approximation
% meanfunc = {@meanSum, {@meanLinear, @meanConst}}; hyp.mean = [0.5; median(x)];
% covfunc = @covSEiso; hyp.cov = [1; 0]; hyp.lik = log(0.1);
% likfunc = @likGauss; sn = 0.1; hyp.lik = log(sn);
% nu = fix(size(x,1)/2); u = linspace(0,142,nu)';
% covfuncF = {@covFITC, {covfunc}, [0; 0]};
% nlm2 = gp(hyp, @infFITC, meanfunc, covfuncF, likfunc, x, y, z);
% [mF s2F] = gp(hyp, @infFITC, meanfunc, covfuncF, likfunc, x, y, z);
% 
% f = [mF+2*sqrt(s2F); flipdim(mF-2*sqrt(s2F),1)];
% fill([z; flipdim(z,1)], f, [7 7 7]/8)
% hold on; plot(z, mF); plot(x, y, '+');

% model 5: neural networks
L = rand(size(x,2),1); 
sf =2;
meanfunc = {@meanSum, {@meanLinear, @meanConst}}; hyp.mean = [0.5; mean(x)];
covfunc = {'covNNone'}; hyp.cov = log([L;sf]);
likfunc = @likGauss; sn = 0.1; hyp.lik = log(sn);
nlml2 = gp(hyp, @infExact,  meanfunc, covfunc, likfunc, x, y)
[m, s2, fmu, fs2, lp] = gp(hyp, @infExact, meanfunc, covfunc, likfunc, x, y, z)

nlml2 = gp(hyp2, @infExact, [], covfunc, likfunc, x, y)
[m s2] = gp(hyp2, @infExact, [], covfunc, likfunc, x, y, z)

figure(1)
f = [m+2*sqrt(s2); flipdim(m-2*sqrt(s2),1)];
fill([z; flipdim(z,1)], f, [7 7 7]/8);
hold on; plot(z, m, 'LineWidth', 2); plot(x, y, '+', 'MarkerSize', 12)
