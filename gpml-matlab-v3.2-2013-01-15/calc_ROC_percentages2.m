function [ results ] = calc_ROC_percentages2( lp, labels, no_noise )
%calc_ROC_percentages: Calculate the precisian and recal for the ROC curve
%   Accepts as inputs the log probabilities and their labels.
    results = ones(size(lp,1),2);
    contor = 1;
    correct_data_size = size(lp,1)-no_noise;
    
    for k=1:size(lp,1)
        threshold = lp(k);
        %what is bellow the threshold is declared noise by our algorithm
        false_noise = 0; % points labeled as noise that aren't
        missed_noise = 0; % points labeled as not noisy that are actually noisy
        
        for i=1:size(lp,1)
            if (lp(i)<threshold)
                % in this case the point should be noisy
                if(labels(i)==-1)
                    % should have been noise, but was labeled not noisy
                    false_noise = false_noise + 1;
                end
            else
                % should be a normal point, without noise             
                if(labels(i)==1)
                    missed_noise = missed_noise + 1;
                end
            end
        end
    results(contor,:) = [false_noise/correct_data_size, (1- missed_noise/no_noise)];
    contor = contor + 1;   
    end
end



