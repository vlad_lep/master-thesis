% 7973 - rascal id project,
x_size = size(activity_train);
test_size = 100;
x= activity_train(:,2:5);
y =activity_train(:,6);
z_rascal = activity_array(601791:601845,2:5);
zs_rascal = activity_array(601791:601845,6);

max_functions = -100;
L = 1; %
sf =2; %signal variance

covfunc = {'covNNone'}; hyp_rascal.cov = log([L;sf]);
likfunc = @likGauss; 
sn = 0.1; % noise variance
hyp_rascal.lik = log(sn);

inference = @infExact;
clock
hyp_rascal = minimize(hyp_rascal, @gp, -max_functions, inference, [], covfunc, likfunc, x, y);
clock
[m_rascal, s2, fmu, fs2, lp_rascal] = gp(hyp_rascal, inference, [], covfunc, likfunc, x, y, z_rascal, zs_rascal);
clock
results_rascal(:,:) = [activity_array(601791:601845,:) lp_rascal(:,1) m_rascal(:,1)];
results_rascal = sortrows(results_rascal, 7);
