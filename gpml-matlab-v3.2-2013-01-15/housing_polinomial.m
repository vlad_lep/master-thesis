close all;
clear all;
load ('housing_exp/housing.mat');

x= train_data(:, 1:13);
y =train_data(:,14);
z = test_data(:, 1:13);
zs = test_data(:,14);
noise_test = noise_data(:,1:13);
noise_test_label = noise_data(:,14);
% http://www.mathworks.com/matlabcentral/answers/47451-how-can-i-compute-regression-coefficients-for-two-or-more-output-variables
%     http://www.mathworks.nl/help/stats/linearmodel.predict.html
p = mvregress(x,y);

z_pred = predict(p,z);       %predicted targets
noise_pred = p(1) *   z + p(2); % predicted noise
errors_z(:) =  abs(z_pred(:) - zs(:));
errors_z(:,2) = -1; % correct point

errors_noise(:) = abs(noise_pred(:) - noise_test_label(:));
errors_noise(:,2) = 1; %noisy point

final_errors = [errors_z; errors_noise]; 
roc_results = calc_ROC_percentages2( final_errors (:,1), final_errors (:,2), errors_noise);

figure(1)
scatter(roc_results(:,1),roc_results(:,2),'.');
xlabel('Percentage of normal points labeled as noise')
ylabel('Percentage of noisy points labeled as noisy')
