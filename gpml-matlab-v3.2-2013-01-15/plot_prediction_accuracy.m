%Plot the accuracy prediction for the activity data.

diff_in_pred = abs(results_activity(:,12) - results_activity(:,10));
max_diff = 1000; % the max(diff_in_pred) is actually 118152.436633508, but this is of no interest for us. We used the next point that is 31837.
n_array = size(results_activity,1);
accuracy = zeros(max_diff,1);
for i= 1:max_diff 
    accuracy(i) = size(find(diff_in_pred<i),1)/ n_array * 100;

end
plot(accuracy);
xlabel('absolute difference between prediction and real value', 'fontsize',14)
 ylabel('percenate', 'fontsize',14)