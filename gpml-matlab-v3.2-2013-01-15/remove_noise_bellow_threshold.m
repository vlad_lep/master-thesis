function [ mew_lp ] = remove_noise_bellow_threshold( lp, weights, threshold )
%REMOVE_NOISE_BELLOW_THRESHOLD Remove points that have a lower +/- 
%percentage variance than the threshold
    new_lp = [];
    contor = 1;
    for i=1:size(lp)
       if weights(lp(i,2))>threshold || weights(lp(i,2))< -threshold
           mew_lp(contor,:) = lp(i,:);
           contor = contor + 1;
       end
    end
end

