clear all, close all
load all_data_with_errors_v3;


for i=1:4
    %size(activity_data,2)
    figure(i)
    project_data = [];  
    for j=1:size(activity_data(i).code_added, 1)
        project_data(j,:) = [j activity_data(i).code_added(j) activity_data(i).code_removed(j) activity_data(i).contributors(j) activity_data(i).commits(j)];
    end
    inner_project(project_data);
end