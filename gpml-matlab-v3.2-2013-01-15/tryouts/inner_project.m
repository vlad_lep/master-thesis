function [ ] = inner_project( all_data)
    %INNER_PROJECT Calculate the probability that this project is correct based
    %on self data. 
    input_rows = [1:2:size(all_data,1)];
    test_rows = [2:2:size(all_data,1)];
    x = all_data(input_rows,4);
    y = all_data(input_rows,5);    
    xtest = all_data(test_rows,4);    
    ytest = all_data(test_rows,5);

    % model 2: Calculate the mean and covariance hyp from training covSEiso
    covfunc = @covSEiso; hyp2.cov = [0; 0]; hyp2.lik = log(0.1);
    likfunc = @likGauss; sn = 0.1; hyp.lik = log(sn);
    hyp2 = minimize(hyp2, @gp, -100, @infExact, [], covfunc, likfunc, x, y);
    exp(hyp2.lik)
    nlml2 = gp(hyp2, @infExact, [], covfunc, likfunc, x, y)
    [m s2 ml s2l lg] = gp(hyp2, @infExact, [], covfunc, likfunc, x, y, xtest, ytest);
    lg
    
  
    f = [m+2*sqrt(s2); flipdim(m-2*sqrt(s2),1)];
    fill([xtest; flipdim(xtest,1)], f, [7 7 7]/8);
    hold on; plot(xtest, m, 'LineWidth', 2); plot(x, y, '+', 'MarkerSize', 12)
end

