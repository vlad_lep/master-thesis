    function [ output_data ] = struct_to_array_activity( activity)
%STRUCT_TO_ARRAY_ACTIVITY Summary of this function goes here
%   Detailed explanation goes here
output_data= [];
contor = 1;
for i=1:size(activity,2)
    for j=1:size(activity(i).code_added,1)
        output_data(contor,:) = [i j activity(i).code_added(j) activity(i).code_removed(j) activity(i).comments_added(j) activity(i).comments_removed(j) activity(i).blanks_added(j) activity(i).blanks_removed(j) activity(i).contributors(j) activity(i).commits(j) ];
        contor = contor+1;
    end
    if mod(i,200) ==0
        i
    end
end
end

