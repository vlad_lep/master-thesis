close all;
clear all;
load ('housing_exp/housing.mat');

x= train_data(:, 1:13);
y =train_data(:,14);
z = test_data(:, 1:13);
zs = test_data(:,14);
noise_test = noise_data(:,1:13);
noise_test_label = noise_data(:,14);

max_functions = -100;

sf =2; %signal variance
covfunc = {@covMaterniso, 3}; ell = 1; hyp.cov = log([ell; sf]);
likfunc = @likGauss; 
sn = 0.1; % noise variance
hyp.lik = log(sn);

hyp = minimize(hyp, @gp, -max_functions, @infExact, [], covfunc, likfunc, x, y);
nlz = gp(hyp, @infExact, [], covfunc, likfunc, x, y, z, zs);
[m, s2, fmu, fs2, lp] = gp(hyp, @infExact, [], covfunc, likfunc, x, y, z, zs);
[m, s2, fmu, fs2, lpnoise] = gp(hyp, @infExact, [], covfunc, likfunc, x, y, noise_test, noise_test_label);

lp(:,2) = 1:size(lp,1);
lp(:,3) = -1; % correct point
lpnoise(:,2) = 1:size(lpnoise,1);
lpnoise(:,3) = 1; %noisy point
final_lp = [lp ; lpnoise];
roc_results = calc_ROC_percentages( final_lp(:,1), final_lp(:,3), size(lpnoise,1));
figure(1)
plot(roc_results(:,1),roc_results(:,2),'--gs','LineWidth',2,...
                'MarkerEdgeColor','g','MarkerFaceColor','g','MarkerSize',5);
xlabel('Percentage of normal points labeled as noise')
ylabel('Percentage of noisy points labeled as noisy')

final_lp = sortrows(final_lp,1);
disp('Number of noisy points in the first 50 most probable'':')
noise_in_first_n(final_lp(:,3),50)
disp('FPR for to indentify more than 50% of noise'':')
FPR = fpr_for_recall_above(roc_results,0.5)



new_lp = remove_noise_bellow_threshold(lpnoise, weights, 0.20);
final_lp = [lp ; new_lp];
roc_results = calc_ROC_percentages( final_lp(:,1), final_lp(:,3), size(new_lp,1));
figure(2)
plot(roc_results(:,1),roc_results(:,2),'--gs','LineWidth',2,...
                'MarkerEdgeColor','g','MarkerFaceColor','g','MarkerSize',5);
xlabel('Percentage of normal points labeled as noise')
ylabel('Percentage of noisy points labeled as noisy')

final_lp = sortrows(final_lp,1);
disp('Number of noisy points in the first 50 most probable after removing noise less than 20% ')
noise_in_first_n(final_lp(:,3),50)
disp('FPR for to indentify more than 50% of noise'':')
FPR = fpr_for_recall_above(roc_results,0.5)



new_lp = remove_noise_bellow_threshold(lpnoise, weights, 0.40);
final_lp = [lp ; new_lp];
roc_results = calc_ROC_percentages( final_lp(:,1), final_lp(:,3), size(new_lp,1));
figure(3)
plot(roc_results(:,1),roc_results(:,2),'--gs','LineWidth',2,...
                'MarkerEdgeColor','g','MarkerFaceColor','g','MarkerSize',5);
xlabel('Percentage of normal points labeled as noise')
ylabel('Percentage of noisy points labeled as noisy')

final_lp = sortrows(final_lp,1);
disp('Number of noisy points in the first 50 most probable after removing noise less than 40% ')
noise_in_first_n(final_lp(:,3),50)
disp('FPR for to indentify more than 50% of noise'':')
fpr_for_recall_above(roc_results,0.5)