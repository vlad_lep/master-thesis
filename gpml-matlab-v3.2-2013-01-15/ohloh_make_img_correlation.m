figure (1);

scatter(size_array(1:10000,3), size_array(1:10000,4))
xlabel('code', 'fontsize',20)
ylabel('comments', 'fontsize',20)

figure (2)
scatter(size_array(1:10000,7), size_array(1:10000,8))
ylabel('man months', 'fontsize',20)
xlabel('commits', 'fontsize',20)

figure (3)
scatter(size_array(1:10000,3), size_array(1:10000,6))
xlabel('code', 'fontsize',20)
ylabel('comment ratio', 'fontsize',20)


figure(4)
scatter(size_array(1:10000,3), size_array(1:10000,5))
ylabel('blanks', 'fontsize',20)
xlabel('code', 'fontsize',20)


% activity facts
figure(5)
scatter(activity_array(1:10000,3), activity_array(1:10000,5))
xlabel('code added', 'fontsize',20)
ylabel('comments added', 'fontsize',20)

figure(6)
scatter(activity_array(1:10000,10), activity_array(1:10000,9))
ylabel('contributors', 'fontsize',20)
xlabel('commits', 'fontsize',20)

figure(7)
scatter(activity_array(1:10000,10), activity_array(1:10000,3))
xlabel('commits', 'fontsize',20)
ylabel('code added', 'fontsize',20)

figure(8)
scatter(activity_array(1:10000,2), activity_array(1:10000,3))
ylabel('code added', 'fontsize',20)
xlabel('month', 'fontsize',20)