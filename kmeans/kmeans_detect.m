function kmeans_detect(data, no_clusters, no_noise)
%% Functian that calculates the centroids of the points and their retrives 
%% the first no_noise most susperct points
%% no_cluster := number of cluster
%% no_noise := the first no_noise most suspect number of noise imputs 
%%


% TODO: K-means clustering results are sensitive to the order of objects in the data set.
% A justified practice would be to run the analysis several times, randomizing objects order; 
% then average the cluster centres of those runs and input the centres as initial ones for one final run of the analysis.
    data  = normalize(data);
    [idx, centroids, sumd ,distances] = kmeans(data, no_clusters);
    for i=1:size(distances,1)
        min_dist_per_point(i,:) = [i,min(distances(i))];
    
    end
    sortrows(min_dist_per_point,2)
    %return last no_noise
    no_noise
end
