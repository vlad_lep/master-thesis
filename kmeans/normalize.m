function [ norm_data ] = normalize( data )
%NORMALIZE Returns the normalized matrix for the given data. 
    for i=1:size(data,2)
        norm_data(:,i) = data(:,i) / norm(data(:,i));
    end
end

